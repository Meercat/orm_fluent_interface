<?php

namespace FluentInterface;

class ActiveRecord
{
    const PRIMARY_KEY = 'id';
    protected $db;
    protected $fields;
    protected $values;
    protected $tableName;

    public function __construct(\PDO $pdo)
    {
        $this->db = $pdo;

        $this->fields = static::fields($pdo);
    }

    public static function fields(\PDO $pdo)
    {
        $table = static::tableName();
        $query = "SHOW COLUMNS FROM $table;";
        return array_column($pdo->query($query)->fetchAll(), 'Field');
    }

    public static function tableName()
    {
        $className = strtolower(get_called_class());
        $path = explode('\\', $className);
        $tableName = end($path);

        return $tableName . 's';
    }

    public static function find(\PDO $pdo)
    {
        return new DbQuery(get_called_class(), $pdo);
    }

    public function __get($name)
    {
        if (array_search($name, $this->fields) !== false) {
            return isset($this->values[$name]) ? $this->values[$name] : null;
        } else {
            throw new \Exception("Column $name is not defined");
        }
    }

    public function __set($name, $value)
    {
        if (array_search($name, $this->fields) !== false) {
            $this->values[$name] = $value;
        } else {
            throw new \Exception("Column $name is not defined");
        }
    }

    public function save()
    {
        if ($this->isNewRecord()) {
            return $this->insert();
        } else {
            return $this->update();
        }
    }

    /**
     * @return bool
     */
    protected function isNewRecord()
    {
        return empty($this->values[self::PRIMARY_KEY]);
    }

    /**
     * @return bool
     */
    protected function insert()
    {
        $strPlaceHolder = '';

        $tableName = static::tableName();
        $columns = join(', ', $this->fields);
        $values = [];
        foreach ($this->values() as $field => $value) {
            if ($value == null) {
                $values[$field] = 'NULL';
            } else {
                $values[$field] =$this->values[$field];
            }
        }
        /*
         * робимо стічку для неіменованих плейсхолдерів
         */
        for($i = 0; $i < count($values); $i++)
        {
            $strPlaceHolder .= '?, ';
        }
        $strPlaceHolder = substr_replace($strPlaceHolder, '', -2);

        /*
         * робимо стрічку SQL запиту
         */
        $sql = sprintf(
            "INSERT INTO %s (%s) VALUES ($strPlaceHolder)",
            $tableName,
            $columns
        );
        /*
         * робимо мисив з параметрами для підстановлення в підготовлений запит до БД
         */
        $bindParams = array_values($values);

        /*
         * Робимо запит до БД
         */
        $statement = $this->db->prepare($sql);
        for($i = 1; $i <= count($bindParams); $i++)
        {
            $statement->bindParam($i, $bindParams[$i-1]);
        }

        $statement->execute();

        $result = $this->db->query('SELECT LAST_INSERT_ID()')->fetch()[0];

        $this->values[self::PRIMARY_KEY] = $result;
        
        return $statement;
    }

    public function values()
    {
        $values = [];
        foreach ($this->fields as $field) {
            if (!isset($this->values[$field])) {
                $values[$field] = null;
            } else {
                $values[$field] = $this->values[$field];
            }
        }
        
        return $values;
    }

    public function update()
    {
        $str = '';
        $tableName = static::tableName();

        foreach ($this->values as $kyes => $values)
        {
            if($kyes != 'id')
            {
            $str .= $kyes . '=:' . $kyes . ', ';
            }
        }

        $updateFields = substr_replace($str, '', -2);

        $sql = "UPDATE $tableName SET $updateFields WHERE id=:id";

        $stmt = $this->db->prepare($sql);

        foreach($this->values as $kyes => $values)
        {
            $stmt->bindParam(':'.$kyes, $this->values[$kyes]);
        }

        $stmt->execute();
    }
}

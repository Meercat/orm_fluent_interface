<?php

namespace FluentInterface;

class DbQuery
{
    protected $model;
    protected $tableName;
    protected $fields;
    protected $join;
    protected $where;
    protected $order;
    protected $limit;
    protected $offset;
    protected $groupBy;
    protected $fetchParam = \PDO::FETCH_OBJ;
    private $db;
    private $bindParam = [];

    public function __construct($model, $pdo)
    {
        $this->model = $model;
        $this->db = $pdo;
        $this->tableName = call_user_func([$model, 'tableName']);
        $this->fields = call_user_func([$model, 'fields'],$pdo);
        $this->fields = join(', ', $this->fields);
    }
    public function getFieldsName()
    {
        return call_user_func([$this->model, 'fields'],$this->db);
    }
    
    public function query()
    {
        $query = sprintf(
            'SELECT %s FROM %s %s %s %s %s %s %s',
            $this->fields,
            $this->tableName,
            $this->join,
            $this->where,
            $this->order,
            $this->limit,
            $this->offset,
            $this->groupBy
        );
        return $query;
    }
    
    public function prepare()
    {
        $stmt = $this->db->prepare(self::query());
        foreach($this->bindParam as $kye => $value)
        {
            $stmt->bindParam(':' . $kye, $value);
        }
        return $stmt;
    }
    /*
     * возвращает одну строку в виде объекта
     */
    public function one()
    {
        try{ 
        $stmt = self::prepare();
        $stmt->execute();
        var_dump($stmt->fetch($this->fetchParam));
        } catch(\PDOException $e){
            throw new \Exception($e->getMessage());
        }   
    }

    /*
     * возвращает массив объектвов
     */
    public function all()
    {
        try{
            $stmt = self::prepare();
            $stmt->execute();
            var_dump($stmt->fetchAll($this->fetchParam));
            
        } catch(\PDOException $e){
            throw new \Exception($e->getMessage());
        }
    }

    /*
     * возвращает указаний столбец результата
     * если номер столбца не указан, то возвращает первий
     * номерация столбцов начинается с '0'
     */
    public function column($param = 0)
    {
        try{
        $stmt = self::prepare();
        $stmt->execute();
        var_dump($stmt->fetchall(\PDO::FETCH_COLUMN, $param));
        } catch(\PDOException $e){
            throw new \Exception($e->getMessage());
        }
    }
    
 /*
 * функция delete() создает SQL запрос подсчета
 * например User::find($pdo)->where('=', 'id', 2)->groupBy('name')->delete();
 * даст следующий SQL запрос: DELETE FROM users WHERE id='2'
 */
    public function delete()
    {
        $query = sprintf(
            'DELETE FROM \'%s\' %s',
            $this->tableName,
            $this->where
        );
        try{
        $stmt = $this->db->query($query);
        } catch(\PDOException $e){
            throw new \Exception($e->getMessage());
        }
    }
/*
 * функция count() создает SQL запрос подсчета
 * например User::find($pdo)->where('>', 'age', 25)->groupBy('name')->count('name');
 * даст следующий SQL запрос: SELECT COUNT(*), name FROM users WHERE age>'25' GROUP BY name 
 */
    public function count($fields)
    {
        $query = sprintf(
            'SELECT COUNT(*), %s FROM %s %s %s',
            $fields,
            $this->tableName,
            $this->where,
            $this->groupBy
        );
        try{
        $stmt = $this->db->query($query);
        } catch(\PDOException $e){
            throw new \Exception($e->getMessage());
        }   
    }

    /*
     * параметри - названия полей из БД, которые надо получить
     * возвращаеть строку fieldName1, fieldName2,...  или '*';
     * ...->select('id', 'name')->... вернет фрагмент SQL запроса в виде
     * ... id, name...
     */
    public function select()
    {
        $fields = '';
        $arrayParams = func_get_args();
        $fieldsArray = explode(', ', $this->fields);

        for($i = 0; $i < count($arrayParams); $i++)
        {
            if(in_array(($arrayParams[$i]), $fieldsArray))
            {
                $fields .= $arrayParams[$i] . ', ';
            } else
            {
                $fields = '';
                break;
            }  
        }
        $this->fields = substr_replace($fields, '', -2);
        if($arrayParams[0] == '*')
        {
            $this->fields = '*';
        }
        return $this;
    }

    /*Формат оператора позволяет задавать произвольные условия в программном стиле. Он имеет следующий вид:
[operator, operand1, operand2, ...]
Операнды могут быть заданы в виде строкового формата, формата массива или формата операторов рекурсивно, в то время как оператор может быть одним из следующих:
    and: операнды должны быть объединены с помощью оператора AND. Например, ['and', 'id=1', 'id=2'] сгенерирует id=1 AND id=2. Если операнд массив, он будет сконвертирован в строку по правилам описанным ниже. Например, ['and', 'type=1', ['or', 'id=1', 'id=2']] сгенерирует type=1 AND (id=1 OR id=2). Этот метод не производит никакого экранирования.
    or: похож на оператор and за исключением того, что будет использоваться оператор OR.
    between: первый операнд должен быть именем столбца, а второй и третий оператор должны быть начальным и конечным значением диапазона. Например, ['between', 'id', 1, 10] сгенерирует id BETWEEN 1 AND 10.
    not between: похож на between за исключением того, что BETWEEN заменяется на NOT BETWEEN в сгенерированном условии.
    in: первый операнд должен быть столбцом или выражением БД. Второй операнд может быть либо массивом, либо объектом Query. Будет сгенерировано условие IN. Если второй операнд массив, он будет представлять набор значений, которым может быть равен столбец или выражение БД; Если второй операнд объект Query, будет сформирован подзапрос, который будет использован как диапазон для столбца или выражения БД. Например, ['in', 'id', [1, 2, 3]] сформирует id IN (1, 2, 3). Метод будет правильно экранировать имя столбца и значения диапазона. Оператор in также поддерживает составные столбцы. В этом случае, первый операнд должен быть массивом названий столбцов, в то время как операнд 2 должен быть массивом массивов или объектом Query представляющим диапазоны для столбцов.
    not in: похож на оператор in, кроме того что IN будет заменён на NOT IN в сформированном условии.
    like: первый операнд должен быть столбцом или выражением БД, а второй операнд будет строкой или массивом представляющим значения, на которые должны быть похожи столбцы или выражения БД. Например, ['like', 'name', 'tester'] сформирует name LIKE '%tester%'. Когда диапазон значений задан в виде массива, несколько LIKE утверждений будут сформированы и соединены с помощью AND. Например, ['like', 'name', ['test', 'sample']] сформирует name LIKE '%test%' AND name LIKE '%sample%'. Вы также можете передать третий необязательный операнд, для указания способа экранирования специальных символов в значениях. Операнд должен быть представлен массивом соответствия специальных символов их экранированным аналогам. Если этот операнд не задан, то будет использовано соответствие по умолчанию. Вы можете также использовать значение false или пустой массив, чтоб указать что значения уже экранированы. Обратите внимание, что при использовании массива соответствия экранирования (или если третий операнд не передан), значения будут автоматически заключены в символы процентов.
    or like: похож на оператор like, только утверждения LIKE будут объединяться с помощью оператора OR, если второй операнд будет представлен массивом.
    >, <=, или другие валидные операторы БД, которые требуют двух операндов: первый операнд должен быть именем столбца, второй операнд это значение. Например, ['>', 'age', 10] сформирует age>10.
*/

    public function where()
    {
        $params = func_get_args();

        switch($params[0])
        {
            case('and'):
            case('or'): $this->where = self::andOr($params); break;
            case('between'):
            case('not between'): $this->where = self::between($params); break;
            case('in'):
            case('not in'): $this->where = self::in($params); break;
            case('like'):
            case('or like'): $this->where = self::like($params); break;
            default: $this->where ='WHERE ' . $params[1] . $params[0] . '\'' . $params[2] . '\'';
        }
        return $this;
    }

    /*
     * and: операнды должны быть объединены с помощью оператора AND. Например, ['and', 'id=1', 'id=2'] сгенерирует id=1 AND id=2.
     *  or: похож на оператор and за исключением того, что будет использоваться оператор OR.
     */

    protected function andOr($param)
    {
        $str = '';
        $operator = array_shift($param);

        for($i = 0; $i < count($param); $i++)
        {
            $str .= strstr($param[$i], '=', true) . '=:' . $operator . ($i+1) . ' ' . strtoupper($operator) . ' ';
            $this->bindParam [$operator . ($i+1)] = substr(strstr($param[$i], '='), 1);
        }
        return 'WHERE ' . substr_replace($str, '', -(strlen($operator) + 2));
    }

    /*
     * between: первый операнд должен быть именем столбца, а второй и третий оператор должны быть начальным и конечным значением диапазона. Например, ['between', 'id', 1, 10] сгенерирует id BETWEEN 1 AND 10.
     *  not between: похож на between за исключением того, что BETWEEN заменяется на NOT BETWEEN в сгенерированном условии.
     */

    protected function between($param)
    {
        $this->bindParam['between1'] = $param[2];
        $this->bindParam['between2'] = $param[3];
        return 'WHERE ' . $param[1] . ' BETWEEN :between1 AND :between2';
    }

    /*
     * in: первый операнд должен быть столбцом или выражением БД. Второй операнд может быть либо массивом, либо объектом Query. Будет сгенерировано условие IN. Если второй операнд массив, он будет представлять набор значений, которым может быть равен столбец или выражение БД; Если второй операнд объект Query, будет сформирован подзапрос, который будет использован как диапазон для столбца или выражения БД. Например, ['in', 'id', [1, 2, 3]] сформирует id IN (1, 2, 3). Метод будет правильно экранировать имя столбца и значения диапазона. Оператор in также поддерживает составные столбцы. В этом случае, первый операнд должен быть массивом названий столбцов, в то время как операнд 2 должен быть массивом массивов или объектом Query представляющим диапазоны для столбцов.
     * not in: похож на оператор in, кроме того что IN будет заменён на NOT IN в сформированном условии.
     */
    protected function in($param)
    {
        $str = '';
        for($i = 0; $i < count($param[2]); $i++)
        {
            $str .= ':in' . ($i+1) . ", ";
            $this->bindParam['in' . ($i+1)] = $param[2][$i];
        }
        $str = substr_replace($str, '', -2);

        if ($param[0] == 'in')
        {
            return 'WHERE '.$param[1].' IN (' . $str . ')';
        }

        return 'WHERE '.$param[1].' NOT IN (' . $str . ')';
    }

    /*
     * like: первый операнд должен быть столбцом или выражением БД, а второй операнд будет массивом представляющим значения, на которые должны быть похожи столбцы или выражения БД. Например, ['like', 'name', 'tester'] сформирует name LIKE '%tester%'. Когда диапазон значений задан в виде массива, несколько LIKE утверждений будут сформированы и соединены с помощью AND. Например, ['like', 'name', ['test', 'sample']] сформирует name LIKE '%test%' AND name LIKE '%sample%'. Вы также можете передать третий необязательный операнд, для указания способа экранирования специальных символов в значениях. Операнд должен быть представлен массивом соответствия специальных символов их экранированным аналогам. Если этот операнд не задан, то будет использовано соответствие по умолчанию. Вы можете также использовать значение false или пустой массив, чтоб указать что значения уже экранированы. Обратите внимание, что при использовании массива соответствия экранирования (или если третий операнд не передан), значения будут автоматически заключены в символы процентов.
     * or like: похож на оператор like, только утверждения LIKE будут объединяться с помощью оператора OR, если второй операнд будет представлен массивом.
     */
    protected function like($param)
    {
        $str = '';
        $andOr = ' OR ';
        if($param[0] == 'like')
        {
            $andOr = ' AND ';
        }

        for($i = 0; $i < count($param[2]); $i++)
        {
            $str .= $param[1] . ' LIKE ' . ' :' . 'like' . ($i+1) . ' ' . $andOr . ' ';
            $this->bindParam['like' . ($i + 1)] .= '%' . $param[2][$i] . '%';
        }

        $str = substr_replace($str, '', -(strlen($andOr) + 2));

        return 'WHERE ' . $str;
    }

    /*
     * join('LEFT JOIN', 'post', 'post.user_id = user.id')
     * результат LEFT JOIN `post` ON `post`.`user_id` = `user`.`id`
     */
    public function join($method, $tableName, $param)
    {
        $this->fields = explode(", ", $this->fields);

        for($i = 0; $i < count($this->fields); $i++)
        {
            $fields[$i] = $this->tableName . '.' . $this->fields[$i];
        }
        $this->fields = join(', ', $fields);
        $this->join = $method . " " . $tableName . " ON (" . $param . ")";
        return  $this;
    }

    /*
     * $condition - string
     * field name in database table
     */
    public function order($condition, $desc = 'ASC')
    {
        $fields = self::getFieldsName();
        if(in_array($condition, $fields) && $desc == 'ASC')
        {
            $this->order = ' ORDER BY ' . $condition . ' ASC';
        } elseif (in_array($condition, $fields) && $desc == 'DESC')
        {
            $this->order = ' ORDER BY ' . $condition . ' DESC';
        } else
        {
            $this->order = 'FALSE';
        }
        return $this;
    }

    /*
     * $condition - integer
     */
    public function limit($condition)
    {
        $this->limit = ' LIMIT :limit';
        $this->bindParam['limit'] = $condition;
        return $this;
    }

    /*
     * $condition - integer
     */
    public function offset($condition)
    {
        $this->offset = ' OFFSET :offset';
        $this->bindParam['offset'] = $condition;
        return $this;
    }

    /*
     * $condition - string
     * field name in database table
     */
    public function groupBy($condition)
    {
        $fields = self::getFieldsName() ;
        if(in_array($condition, $fields))
        {
            $this->groupBy = ' GROUP BY ' . $condition;
        } else
        {
            $this->groupBy = 'FALSE';
        }
        return $this;
    }

    /*
     * Запрос User::find($pdo)->asArray()->all();
     * возвращает результат в виде ассоциативного массива
     */
    public function asArray()
    {
        $this->fetchParam = \PDO::FETCH_ASSOC;
        return $this;
    }
}

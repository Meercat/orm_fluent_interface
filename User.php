<?php

namespace FluentInterface;

/**
 * Class User
 * @package FluentInterface
 *
 * @property $id
 * @property $name
 * @property $age
 * @property $email
 *
 */
class User extends ActiveRecord
{
}

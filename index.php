<?php
error_reporting(E_ALL);
include_once 'DbQuery.php';
include_once 'ActiveRecord.php';
include_once 'Product.php';
include_once 'User.php';

use \FluentInterface\User;
use \FluentInterface\Product;

//$query = (new User())
//    ->fields(['name', 'age'])
//    ->tableName(['users'])
//    ->where(['id = 1'])->get();


//$insert = (new User())
//    ->tableName(['users'])
//    ->fields(['name', 'age', 'email'])
//    ->values(['bob', 30, 'bbb@bbb.bb']);

$dsn = "mysql:host=localhost; dbname=orm_test";
$pdo = new \PDO($dsn, 'root', 'root');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);


//$users = User::find($pdo)->where('=', 'age', '25\'; TRUNCATE TABLE users;\'')->all();

User::find($pdo)->where('between', 'id', 2, 4)->all();
//User::find($pdo)->where('and', 'age=36', 'name=Pit')->all();
//User::find($pdo)->select('name')->join('LEFT JOIN', 'orders', 'users.id=orders.user_id')->asArray()->one();
//User::find($pdo)->asArray()->select('*')->limit(3)->offset(3)->order('id', 'DESC')->column();

//$user = new User($pdo);
//$user->id = 1;
//$user->name = 'Nick';
//$user->age = 25;
//$user->email = 'ppp@ppp.pp"); TRUNCATE TABLE users;"';
//$user->email = 'Nick@nnn.nn';
//$user->save();

